#include <Windows.h>
#include <lmcons.h>
#include <lmserver.h>
#include <stdio.h>


void copyDNS(char* dst,char* org){
	while( org[0] || org[1] ){
		dst[0]=org[0];	
		org+=2;
		dst+=1;
	}
	*dst=0;
}

int main(void){
	LPBYTE buf=NULL;
	unsigned long i,eread,totale;
	char Name[64],Comment[512];
	long bufsize=1500*sizeof(SERVER_INFO_101);
	buf=(LPBYTE)malloc(bufsize);
	long ret=NetServerEnum(NULL,101,&buf,bufsize,&eread,&totale,0x1000,NULL,0);
	for(i=0;i<eread;i++){
		copyDNS(Name,   ((SERVER_INFO_101*)buf)[i].sv101_name);
		copyDNS(Comment,((SERVER_INFO_101*)buf)[i].sv101_comment);
		printf("%-14s %-58s\n",Name,Comment);
	}
	return 0;
}
